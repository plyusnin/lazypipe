## LAZYPIPE

### Description 

Lazypipe is a bioinformatic pipeline for analyzing virus and bacteria metagenomics from NGS data.

![Lazypipe flowchart](https://bitbucket.org/plyusnin/lazypipe/wiki/img/LazypipeFlowchart.v3.0.png)

**Lazypipe supports:**

* fastq preprocessing
* de novo assembling
* taxonomic binning
* taxonomic profiling
* reporting
	* mapped contigs sorted by taxa
	* virus contigs
	* unmapped contigs
	* contig annotations (tsv and excel)
	* taxon abundancies (tsv, excel and KronaGraph)
    * quality control plots


**Quick access on CSC**

Lazypipe can be quickly assessed using a [preinstalled module](https://docs.csc.fi/apps/lazypipe/) at the [Finnish Center of Scientific Computing](https://research.csc.fi/). 

**User Guide**

Detailed [**User Guide**](https://bitbucket.org/plyusnin/lazypipe/wiki/UserGuide.v3.0.md) with examples. 

**Citing Lazypipe**

1. Plyusnin Ilya, Olli Vapalahti, Tarja Sironen, Ravi Kant, and Teemu Smura. â€œEnhanced Viral Metagenomics with Lazypipe 2.â€ Viruses 15, no. 2 (February 4, 2023): 431. https://doi.org/10.3390/v15020431

1. Ilya Plyusnin, Ravi Kant, Anne J. Jaaskelainen, Tarja Sironen, Liisa Holm, Olli Vapalahti, Teemu Smura. (2020) Novel NGS Pipeline for Virus Discovery from a Wide Spectrum of Hosts and Sample Types. Virus Evolution, veaa091, https://doi.org/10.1093/ve/veaa091

**LICENSE**

GNU GPLv3

See LICENSE and COPYRIGHT

**Contact**

Project website: https://www.helsinki.fi/en/projects/lazypipe

Contact email: grp-lazypipe@helsinki.fi

