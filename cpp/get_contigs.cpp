#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
//#include <cstring>

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <getopt.h>
#include <seqan/seq_io.h>


/* LAZYPIPE PROJECT: C++ CODE FOR NGS PIPELINE (2022)
 *
 * Retrieve contigs mapped to a given taxid based on results in resdir
 *
 * Author: Ilya Plyusnin, University of Helsinki (2022)
 * Creadit: Lazypipe project, https://doi.org/10.1093/ve/veaa091
 */
using namespace std;

void print_usage(const string name){
	std::cerr << "\nUSAGE: "<< name <<" -t taxid(s) [-r resdir -v] 1> contigs.fa\n"
		<<"\n"
		<<"Retrieve contigs assigned to a given taxid\n"
		<<"\n"
		<<"-t str            : taxon id (or comma-separated list of ids). Please use species/genus-level taxids or staxids from contigs.annot-table\n"
		<<"-r dir            : directory with pipeline results. Default: results. MUST include files: contigs.annot.tsv + contings.fa\n"
		<<"-v                : verbal mode. Default: false\n"
		<<"-h                : print this user manual\n"
		<<"\n"
		<<"output            : retrieved contigs are printed to STDOUT\n"
		<<"\n\n"
		<<"Credit: Lazypipe project, https://doi.org/10.1093/ve/veaa091\n\n"
		<<"\n";
}

inline std::vector<std::string> split(const std::string& str, const char delim)
{
    std::stringstream ss {str};
    std::string item;
    std::vector<std::string> result {};
    while (std::getline(ss, item, delim)){
    	if( isspace(item.back()) ){
		item.erase(item.length()-1,1);
	}
    	result.emplace_back(item);
	}

    return result;
}

/*
 * Returns string prefix up to the first occurance of delimiter
 */
inline std::string get_prefix(const std::string& str, const char delim){
	
	unsigned int pos = str.find_first_of(delim);
	if(pos > str.length()){
		return str;
	}
	else{
		return str.substr(0,pos);
	}
}

inline std::string get_suffix(const std::string& str, const char delim){
	unsigned int pos = str.find_first_of(delim);
	if(pos > str.length()){
		return str;
	}
	else{
		return str.substr(pos+1, std::string::npos);
	}
}


// Test file for accessibility/existance
inline bool exists (const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

/**
  * Returns column index corresponding to column name.
  *
  * file 		tsv-file with headers
  * colname     colname to search for
  * returns     colindex or -1 if colname was not found
  */
unsigned int get_colname_ind(const string file, const string colname){
	ifstream in(file, ios::in);
	if(!in.is_open()){
		std::cerr << "\nERROR: failed to open \'"<<file<<"\'\n";
		exit(1);
	}
	char buffer[10000];
	vector<string> sp;
	while(in.getline(buffer,10000)){
		sp = split(string(buffer),'\t');
		for(unsigned int i=0 ; i<sp.size(); i++){
			if( sp[i]==colname){
				in.close();
				return i;
			}
		}
		std::cerr << "WARNING: tsv file "<<file<<" has no colname "<<colname<<"\n";
		in.close();
		return -1;
	}
	return -1;
}	


/*
 * Reads two columns from tsv-file into a map-map structure
 * 
 * file          tsv-file
 * keyi          column containing keys
 * vali          column containing values
 *
 * For example read contig-ids for each taxid:
 * readto_map_map("contig_taxid_score.tsv", 1, 0, taxid_contigmap)
 */
void readto_map_map(const string file, unsigned int keyi, unsigned int vali, unordered_map<string,unordered_map<string,bool> > &map_map){

	ifstream in(file, ios::in);
	if(!in.is_open()){
		std::cerr << "\nERROR: failed to open \'"<<file<<"\'\n"; exit(1);
	}
	char buffer[10000];
	vector<string> sp;
	while(in.getline(buffer,10000)){
		if( (buffer[0] == '#') ||  buffer[0]== '@'){
			continue;
		}
		sp = split(string(buffer),'\t');
		if(  keyi<sp.size() && vali<sp.size()){
			if( map_map.count(sp[keyi]) == 0){
				unordered_map<string,bool> map;
				map_map[sp[keyi]] = map;
			}
			map_map[sp[keyi]][sp[vali]] = true;
		}	
	}
	in.close();
}

int main(int argc, char** argv) {
	
	string prog_name			= string(argv[0]);	
	// paramenters
	string taxid				= "";
	string resdir				= "results";
	string contigs_file			= "";
	string contigs_annot		= "";
	bool verbal					= false;
	bool help					= false;
	
	// PARSING PARAMETERS
	int option= 0;
	while ((option = getopt(argc, argv,"t:c:r:p:1:2:vh")) != -1) {
        switch (option) {
		case 't':
			taxid	= string(optarg);
			break;
		case 'r':
			resdir	= string(optarg);
			break;
		case 'v':
			verbal= true;
			break;
		case 'h':
			help = true;
			break;
		case '?':
        		std::cerr << "option -"<<optopt<<" requires an argument\n\n";
        		exit(1);
             	default:
	     		print_usage(prog_name); 
                	exit(1);
        	}
    	}
	if(help){
		print_usage(prog_name);
		exit(1);
	}
	if(taxid==""){ 	
		std::cerr<<"ERROR: please specify -t taxid\n"; print_usage(prog_name); exit(1);
	}
	if(resdir==""){ 		
		std::cerr<<"ERROR: please specify -r resdir\n\n"; print_usage(prog_name); exit(1);
	}

	// contid_taxid_[score] file
	if( exists(resdir+"/contigs.annot.tsv") ){
		contigs_annot = resdir+"/contigs.annot.tsv";
	}
	else{
		std::cerr << "\nERROR: missing file: "<< (resdir+"/contigs.annot.tsv") << "\n\n";
		exit(1);
	}
	// contig file
	if( exists(resdir+"/contigs.fa") ){
		contigs_file = resdir+"/contigs.fa";
	}
	else{
		std::cerr << "\nERROR: missing file: "<< (resdir+"/contigs.fa") << "\n\n";
		exit(1);
	}
		

	// Listing taxid(s)
	vector<string> taxid_list 				= split(taxid,',');
	unordered_map<string,bool> taxid_set;
	for (auto it = taxid_list.begin(); it != taxid_list.end(); ++it) {
    	taxid_set[*it] = true;
	}

	// Listing contid(s)
	vector<string> contid_list = vector<string>();
	if(verbal){ 
		std::cerr << "\t# reading "<< contigs_annot << "\n";
	}
	unsigned int contid_col 		= get_colname_ind(contigs_annot, "contig");	
	unsigned int taxid_sp_col  		= get_colname_ind(contigs_annot, "species_id");
	unsigned int taxid_ge_col		= get_colname_ind(contigs_annot, "genus_id");
	unsigned int staxid_col 		= get_colname_ind(contigs_annot, "staxid");
	if(staxid_col < 0 || contid_col < 0 || taxid_sp_col < 0  || taxid_ge_col < 0){
		std::cerr << "\nERROR: invalid file: "<<contigs_annot<<"\n";
		exit(1);
	}	
	ifstream in(contigs_annot, ios::in);
	if(!in.is_open()){
		std::cerr << "\nERROR: failed to open \'"  <<  contigs_annot << "\'\n";
		exit(1);
	}
	char buffer[10000];
	vector<string> sp;
	while(in.getline(buffer,10000)){
		if(buffer[0] == '#'){
			continue;
		}
		sp = split(string(buffer),'\t');
		if(  staxid_col<sp.size() && contid_col<sp.size() && taxid_sp_col<sp.size()  && taxid_ge_col<sp.size() ){
			//std::cerr <<"\ttaxid: \""<<sp[taxid_col]<<"\"\n";
			if(    taxid_set.count(sp[staxid_col])>0
				|| taxid_set.count(sp[taxid_sp_col])>0  
				|| taxid_set.count(sp[taxid_ge_col])>0  ){
				contid_list.push_back(sp[contid_col]);
			}
		}
	}
	in.close();
	if(verbal){
		std::cerr << "\t# contigs mapped to requested taxid(s): "<<contid_list.size()<<"\n";
		//for (auto it = contid_list.begin(); it != contid_list.end(); ++it) {
    	//	std::cerr << "\t" << *it<<"\n";
		//}	
	}
	if(contid_list.size() == 0){
		exit(0);
	}
	
	// convert to a set (implemented as a map)
	unordered_map<string,bool> contid_set;
	for (auto it = contid_list.begin(); it != contid_list.end(); ++it) {
    	contid_set[*it] = true;
	}
	

	// Retrieving Contigs by id
	
	if(verbal){ 
		std::cerr << "\t# reading "<<contigs_file<<"\n";
	}
	
	seqan::SeqFileIn fileIn( contigs_file.c_str() );
	seqan::StringSet<seqan::CharString> ids;
	seqan::StringSet<seqan::IupacString> seqs;
	unsigned int batch_size	= 1000;
	//unsigned int report_batch= batch_size*1000;
	unsigned int seq_read	= 0;
	unsigned int seq_sel	= 0;
	
	while(!atEnd(fileIn) ){
		try{
			readRecords(ids,seqs,fileIn,batch_size);
			seq_read += length(seqs);
			
			//writeRecords(fileOut,ids,seqs);
			for(unsigned int ind=0; ind<length(ids); ind++){
				seqan::CharString id 	= ids[ind];
				std::string id2	= std::string( toCString(id) );
				
				if(length(id) == 0){
					std::cerr << "WARNING: skipping seq with an empty id\n";
					continue;
				}
				id2 = get_prefix(id2,'_');
				id2 = get_suffix(id2,'=');
				// DEBUG
				//std::cerr << "id: '"<<id << "'\nid2: '"<< id2 <<"'\n"; exit(1);
				
				
				if(  contid_set.count(id2) > 0 ){
					//writeRecord(fileOut,ids[ind],seqs[ind]);
					//std::cerr << "\tfound contig: "<<id<<"\n";
					std::cout << ">"<<id2 <<"\n"
									<<seqs[ind]<<"\n";
					seq_sel++;
				}
			}
			clear(ids);
			clear(seqs);	
		}
		catch (seqan::IOError const & e){
			std::cerr << "ERROR: IOError:\n"<<e.what()<<"\n";
			return 1;}
		catch(seqan::ParseError const &e){
			std::cerr << "ERROR: badly formatted record:\n"<<e.what()<<"\n";
			return 1;
		}
	}	
	
}
